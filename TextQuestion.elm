module TextQuestion exposing ( Model, Msg, init, update, view )

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)


-- Model

type alias Model =
  { name : String
  , text : String
  , response : Maybe String
  }

init : String -> String -> Model
init name text =
  { name = name
  , text = text
  , response = Nothing
  }

-- Update

type Msg
  = Response String
  | Submit

update : Msg -> Model -> Model
update msg model =
  case msg of
    Response newResponse ->
      { model | response = Just newResponse }

    Submit ->
      model


-- View

view : Model -> Html Msg
view model =
  div [ class "ui segment" ]
    [ p [] [ text model.text ]
    , div [ class "ui fluid input" ]
      [ input [ type' "text", placeholder "Replace me", onInput Response ] []
      ]
    ]
