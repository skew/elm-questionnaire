module Main exposing (main)

import Html exposing (..)
import Html.App as Html
import Html.Attributes exposing (..)

import Question


main : Program Never
main =
  Html.beginnerProgram { model = init, update = update, view = view }


-- Model

type alias Model =
  { title : String
  , questionnaire : Questionnaire
  }

type alias Questionnaire = List Question.Model

init : Model
init =
  { title = "My Questionnaire"
  , questionnaire =
    [ Question.textQuestion
        "user_name"
        "What is your name?"
    , Question.textQuestion
        "user_color"
        "What is your favorite color?"
    , Question.textQuestion
        "user_food"
        "What is your favorite food?"
    , Question.textQuestion
        "user_location"
        "Where do you call home?"
    ]
  }


-- Update

type Msg
  = Update String Question.Msg
  | Submit

update : Msg -> Model -> Model
update msg model =
  case msg of
    Update name msg' ->
      { model | questionnaire = List.map (updateResponse name msg') model.questionnaire }

    Submit ->
      model

updateResponse : String -> Question.Msg -> Question.Model -> Question.Model
updateResponse name msg model =
  if name == (Question.name model) then Question.update msg model else model


-- View

view : Model -> Html Msg
view model =
  div [ class "ui container" ]
    [ stylesheet "https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.min.css"
    , h1 [ class "ui header" ]
      [ i [ class "file icon" ] []
      , text "elm-questionnaire!"
      ]
    , p [] [ text "Easy questionnaires so you don't have to :-)" ]
    , div [ class "ui two column very relaxed stackable grid" ]
      [ div [ class "column" ]
        [ div [] (List.map viewQuestion model.questionnaire)
        , div [ class "ui segment" ] [ button [ class "ui primary button" ] [ text "Generate" ]]
        ]
      , div [ class "ui vertical divider" ] []
      , div [ class "columin" ]
        [ div [ class "ui segment" ] (List.map viewResult model.questionnaire)
        ]
      ]
    ]

viewQuestion : Question.Model -> Html Msg
viewQuestion model =
  model
    |> Question.view
    |> Html.map (\x -> Update (Question.name model) x)

viewResult : Question.Model -> Html Msg
viewResult model =
  let
    answer = Maybe.withDefault "Nothing" (Question.getResponse model)
  in
    div [ class "ui segment" ] [ text (Question.name model ++ ": " ++ answer) ]

stylesheet : String -> Html Msg
stylesheet url =
  node "link"
    [ rel "stylesheet"
    , href url
    ] []
