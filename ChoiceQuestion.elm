module ChoiceQuestion exposing ( Model, Msg, init, update, view )

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)


-- Model

type alias Model =
  { name : String
  , text : String
  , choices : List String
  , response : Maybe String
  }

init : String -> String -> List String -> Model
init name text choices =
  { name = name
  , text = text
  , choices = choices
  , response = Nothing
  }


-- Update

type Msg
  = Response String
  | Submit

update : Msg -> Model -> Model
update msg model =
  case msg of
    Response response ->
      { model | response = Just response }

    Submit ->
      model


-- View

view = viewSelect

viewDiv : Model -> Html Msg
viewDiv model =
  div [ class "ui segment" ]
    [ p [] [ text model.text ]
    , div [ class "ui selection dropdown" ]
      [ input [ type' "hidden", name model.name, onInput Response ] []
      , i [ class "dropdown icon" ] []
      , div [ class "default text" ] [ text "Replace me" ]
      , div [ class "menu" ] (List.map viewOption model.choices)
      ]
    ]

viewSelect : Model -> Html Msg
viewSelect model =
  div [ class "ui segment" ]
    [ p [] [ text model.text ]
    , select []
      ((option [ value "" ] [ text "Replace Me" ]) :: (List.map viewOption2 model.choices))
    ]

viewOption : String -> Html Msg
viewOption choice =
  div [ class "item", attribute "data-value" choice ] [ text choice ]

viewOption2 : String -> Html Msg
viewOption2 choice =
  option [ value choice ] [ text choice ]
