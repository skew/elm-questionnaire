module Question exposing ( Model, Msg, textQuestion, choiceQuestion, name, getResponse, update, view )

import Html exposing (..)
import Html.App as Html

import TextQuestion
import ChoiceQuestion


-- Model

type Model
  = TextQuestionModel TextQuestion.Model
  | ChoiceQuestionModel ChoiceQuestion.Model

textQuestion : String -> String -> Model
textQuestion name text =
  TextQuestionModel (TextQuestion.init name text)

choiceQuestion : String -> String -> List String -> Model
choiceQuestion name text choices =
  ChoiceQuestionModel (ChoiceQuestion.init name text choices)

name : Model -> String
name model =
  case model of
    TextQuestionModel model' ->
      model'.name

    ChoiceQuestionModel model' ->
      model'.name

getResponse : Model -> Maybe String
getResponse model =
  case model of
    TextQuestionModel model' ->
      model'.response

    ChoiceQuestionModel model' ->
      model'.response


-- Update

type Msg
  = TextQuestionMsg TextQuestion.Msg
  | ChoiceQuestionMsg ChoiceQuestion.Msg

update : Msg -> Model -> Model
update msg model =
  case (msg, model) of
    (TextQuestionMsg msg', TextQuestionModel model') ->
      TextQuestionModel (TextQuestion.update msg' model')

    (ChoiceQuestionMsg msg', ChoiceQuestionModel model') ->
      ChoiceQuestionModel (ChoiceQuestion.update msg' model')

    _ -> model


-- View

view : Model -> Html Msg
view model =
  case model of
    TextQuestionModel question ->
      Html.map (\x -> TextQuestionMsg x) (TextQuestion.view question)

    ChoiceQuestionModel question ->
      Html.map (\x -> ChoiceQuestionMsg x) (ChoiceQuestion.view question)
